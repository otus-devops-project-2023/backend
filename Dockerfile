FROM webdevops/php-nginx:7.4

RUN apt update && \
    apt install -y curl \
    net-tools

WORKDIR /app

RUN echo "" > /opt/docker/etc/nginx/vhost.conf
COPY ./nginx/app/vhost.conf /opt/docker/etc/nginx/conf.d/default.conf

COPY ./mailer/composer.json ./composer.json
RUN composer config --no-plugins allow-plugins.php-http/discovery true && \
    composer install

COPY ./mailer .
