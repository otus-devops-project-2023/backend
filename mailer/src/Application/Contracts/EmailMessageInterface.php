<?php

namespace App\Application\Contracts;

interface EmailMessageInterface
{
    public function getTo(): string;

    public function getSubject(): string;

    public function getTextContent(): string;

    public function getHtmlContent(): string;
}