<?php

namespace App\Application\Actions\Email;

use App\Application\Contracts\EmailMessageInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class SendEmailAction
{
    private ParameterBagInterface $parameterBag;

    private MailerInterface $mailer;

    public function __construct(
        ParameterBagInterface $parameterBag,
        MailerInterface $mailer
    ) {
        $this->parameterBag = $parameterBag;
        $this->mailer = $mailer;
    }

    public function execute(EmailMessageInterface $message)
    {
        $email = (new Email())
            ->from($this->parameterBag->get('app.email.from'))
            ->to($message->getTo())
            ->subject($message->getSubject())
            ->text($message->getTextContent())
            ->html($message->getHtmlContent())
        ;

        $this->mailer->send($email);
    }
}
