<?php

namespace App\Controller;

use App\Form\EmailMessageFormType;
use App\Message\SendEmailMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function home(
        Request $httpRequest,
        MessageBusInterface $messageBus
    ) {
        $form = $this->createForm(EmailMessageFormType::class, []);

        $form->handleRequest($httpRequest);

        $responseData = [
            'form' => $form->createView(),
            'success' => null
        ];

        if ($form->isSubmitted()) {
            $data = $form->getData();

            $messageBus->dispatch(
                new SendEmailMessage(
                    $data['to'],
                    $data['subject'],
                    $data['message'],
                    sprintf('<p>%s</p>', $data['message'])
                )
            );

            $responseData['form'] = $this->createForm(EmailMessageFormType::class, [])->createView();
            $responseData['success'] = 'Сообщение успешно отправлено';
        }

        return $this->render('home.html.twig', $responseData);
    }
}
